﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OTUS.Reflection
{
	public class O
	{
		int i1;
		int i2;
		int i3;
		int i4;
		int i5;

		public O Get() => new O() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };

		public override string ToString() => $"i1:{i1}, i2:{i2}, i3:{i3}, i4:{i4}, i5:{i5}";
	}

}
