﻿using System;
using System.Reflection;
using System.Text;

namespace OTUS.Reflection
{
	class CustomSerializer<T>
	{
		internal string ToCSV(T obj)
		{
			Type type = obj.GetType();
			var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic);

			var stringBuilder = new StringBuilder();
			foreach (var field in fields)
			{
				stringBuilder.Append($"{field.Name}:{field.GetValue(obj)};");
			}

			return stringBuilder.ToString();
		}

		internal T FromCSV(string csv)
		{
			var type = typeof(T);
			var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic);
			var instance = Activator.CreateInstance(type);

			var strings = csv.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
			if (fields.Length != strings.Length)
				throw new InvalidCastException("Упс, число полей в десериализуемой строке должно быть равно числу полей в классе");

			for (int i = 0; i < fields.Length; i++)
			{
				var pair = strings[i].Split(':');
				var field = type.GetField(pair[0], BindingFlags.Public | BindingFlags.NonPublic);

				field.SetValue(instance, Convert.ChangeType(pair[1], field.FieldType));
			}

			return (T)instance;
		}
	}
}
