﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace OTUS.Reflection
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello OTUS!");
			try
			{
				Stopwatch stopwatchReflectionSer = new Stopwatch();
				Stopwatch stopwatchReflectionDeser = new Stopwatch();
				Stopwatch stopWatchJsonSer = new Stopwatch();
				Stopwatch stopWatchJsonDeser = new Stopwatch();

				var rnd = new Random();

				for (int i = 0; i < 1000000; i++)
				{
					var o = new O();

					var customSerializer = new CustomSerializer<O>();
					stopwatchReflectionSer.Start();
					string serialized = customSerializer.ToCSV(o);
					stopwatchReflectionSer.Stop();

					stopwatchReflectionDeser.Start();
					var desrialized = customSerializer.FromCSV(serialized);
					stopwatchReflectionDeser.Stop();

					stopWatchJsonSer.Start();
					string serializedByJson = JsonConvert.SerializeObject(o);
					stopWatchJsonSer.Stop();

					stopWatchJsonDeser.Start();
					O desrializedByJson = JsonConvert.DeserializeObject<O>(serializedByJson);
					stopWatchJsonDeser.Stop();
				}

				Console.WriteLine();
				Console.WriteLine("Reflection serialize time: " + stopwatchReflectionSer.ElapsedMilliseconds);
				Console.WriteLine("Reflection deserialize time: " + stopwatchReflectionDeser.ElapsedMilliseconds);
				Console.WriteLine("Json serialize time: " + stopWatchJsonSer.ElapsedMilliseconds);
				Console.WriteLine("Json deserialize time: " + stopWatchJsonDeser.ElapsedMilliseconds);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}


			Console.ReadLine();
		}
	}
}
